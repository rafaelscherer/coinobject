const coin = {
    state: 0,
    flip: function () {
        // 1. Um ponto: Randomicamente configura a propriedade “estado” do 
        // seu objeto moeda para ser um dos seguintes valores:
        // 0 ou 1: use "this.state" para acessar a propriedade "state" neste objeto.

        this.state = Math.round(Math.random());
    },
    toString: function () {
        // 2. Um ponto: Retorna a string "Heads" ou "Tails", dependendo de como
        //  "this.state" está como 0 ou 1.
        if (this.state === 0) {
            return "Heads";
        }
        else {
            return "Tails";
        }
    },
    toHTML: function () {
        const image = document.createElement('img');
        // 3. Um ponto: Configura as propriedades do elemento imagem 
        // para mostrar a face voltada para cima ou para baixo dependendo
        // do valor de this.state está 0 ou 1.
        if (this.state === 0) {
            image.src = './images/cara.png';
            image.style.width = '80px';

        } else {
            image.src = './images/coroa.png';
            image.style.width = '80px';
        }

        return image;
    }
};

function display20Flips() {
    const results = [];
    // 4. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma string na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
    for (let i = 0; i < 20; i++) {
        coin.flip();
        let result = coin.toString();
        let span = document.createElement("span");
        span.textContent = result + " ";
        document.body.appendChild(span);
        results.push(result);
    }

    let array = document.createElement("p");
    array.textContent = JSON.stringify(results);
    document.body.appendChild(array);

}
display20Flips();


function display20Images() {
    const results = [];
    // 5. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma imagem na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
    for (let i = 0; i < 20; i++) {
        coin.flip();
        let result = coin.toString();
        let image = coin.toHTML();
        document.body.appendChild(image);
        results.push(result);
    }

    let array = document.createElement("p");
    array.textContent = JSON.stringify(results);
    document.body.appendChild(array);
}
display20Images();
